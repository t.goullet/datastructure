# DataStructure

TP 3 – Structure de données
language : C

GOULLET Thibaut / CARTELIER Alexis

## Objectif du TP

L'objectif de ce TP est d'implémenter quatres algorithmes de tri. Le tri à bulle, le tri par insertion, le tri par séléction et le tri par tas. Ces quatres algortihmes sont capables d'effectuer un tri croissant ou décroissant d'un tableau de nombre à virgules.
Il sera également possible de mesurer le temps d'exécution de chacun des algorithmes selon la taille du tableau.
Les tests effectués permettront de créer un graphique des temps d'exécution.

## Usage des commandes



### Liste de tableaux

Arraylist utilise un tableau qui stocke les données. Lorsqu'une donnée est insérée, la taille du tableau s'ajuste. 

### Liste par tas

Un tas est une structure de données spéciale basée sur un arbre dans laquelle l'arbre est un arbre binaire complet.

### Liste par queue

Renvoie une liste de vos files d'attente dans la région actuelle. La réponse comprend un maximum de 1 000 résultats. Il suit le principe FILO (First In Last Out - premier rentré, dernier sorti). Il suit le même système que pour une file d'attente, le premier arrivé sera le premier à passer et l'ordre de passage se fait dans l'ordre d'arrivée.

### Liste par pile

Une Pile, Stack en anglais, est une structure de données qui suit le principe LIFO (Last In First Out - dernier entré, premier sorti). En d'autres termes: on contraint une structure de données dans laquelle l'élément inséré en dernier doit être le premier élément à sortir.
Ce concept limitant est extrêmement utile et permet de manipuler facilement des structures représentant celles de notre monde réel. Un pile représentera par exemple parfaitement une pile d'assiettes ou de livres dans laquelle nous ne pouvons retirer/ajouter, à chaque fois, uniquement l’élément au dessus de la pile.
