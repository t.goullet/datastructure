var searchData=
[
  ['data_10',['data',['../struct_array__list.html#a43ceef2593627dfedfd5b55ee9a8761a',1,'Array_list::data()'],['../struct_heap.html#ac5cbb8d8b1c5becee82f849bbd36d100',1,'Heap::data()'],['../struct_queue.html#a712b110098ccfa5b2d6b636ed199794b',1,'Queue::data()'],['../struct_stack.html#a154ea9e47b3e5d6833c0c401754b1677',1,'Stack::data()']]],
  ['dequeue_11',['dequeue',['../queue_8c.html#a963c35fdb8a91292169e3c65dfa6b56b',1,'dequeue(Queue *q):&#160;queue.c'],['../queue_8h.html#a963c35fdb8a91292169e3c65dfa6b56b',1,'dequeue(Queue *q):&#160;queue.c']]],
  ['dup_12',['dup',['../stack_8c.html#a5df3fde62e5dca9a0e26ad20fe3261f3',1,'dup(Stack *s):&#160;stack.c'],['../stack_8h.html#a5df3fde62e5dca9a0e26ad20fe3261f3',1,'dup(Stack *s):&#160;stack.c']]]
];
