var searchData=
[
  ['peek_32',['peek',['../heap_8h.html#af684013e1c65b2beb9c21efef6baf505',1,'heap.h']]],
  ['peek_5fstack_33',['peek_stack',['../stack_8c.html#ae7cb25b94bcf7fd9acb9fbb0d029a27c',1,'peek_stack(Stack *s):&#160;stack.c'],['../stack_8h.html#ae7cb25b94bcf7fd9acb9fbb0d029a27c',1,'peek_stack(Stack *s):&#160;stack.c']]],
  ['pop_34',['pop',['../heap_8h.html#a962611a81a4cd5a8b824c1a271f75a3e',1,'heap.h']]],
  ['pop_5fstack_35',['pop_stack',['../stack_8c.html#ae061732709da8b2f34f5c0fd4ef0ad5d',1,'pop_stack(Stack *s):&#160;stack.c'],['../stack_8h.html#ae061732709da8b2f34f5c0fd4ef0ad5d',1,'pop_stack(Stack *s):&#160;stack.c']]],
  ['push_36',['push',['../heap_8h.html#a7394afbcf1f644c42ed4f381344e0c89',1,'heap.h']]],
  ['push_5fstack_37',['push_stack',['../stack_8c.html#a43347ab7de63ecfb72ee0472db11f27f',1,'push_stack(Stack *s, float value):&#160;stack.c'],['../stack_8h.html#a43347ab7de63ecfb72ee0472db11f27f',1,'push_stack(Stack *s, float value):&#160;stack.c']]]
];
