#include "array_list.h"
#include <stdlib.h>
#include <assert.h>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @brief initialise une liste
 * 
 * @param l 
 */
void init_array_list(Array_list *l){
    l->index = 0;
}

/**
 * @brief ajouter une valeur en fonctiond de la position
 * 
 * @param l 
 * @param position 
 * @param value 
 */
void insert_at(Array_list *l, int position, float value)
{
    if (position < l->index)
    {
        for (int i = position; i < l->index; i++)
        {
            l->data[i + 1] = l->data[i];
        }
        l->data[position] = value;
        l->index++;
    }
}

/**
 * @brief ajout une valeur a la suite de la liste
 * 
 * @param l 
 * @param value 
 */
void add_list(Array_list *l, float value)
{
    if (l->index < ARRAY_LIST_MAX_SIZE)
    {
        l->data[l->index++] = value;
    }
}

/**
 * @brief supression de la valeur 
 * 
 * @param l 
 * @param position 
 * @return float 
 */
float remove_at(Array_list *l, int position)
{
    float tmp = l->data[position];
    l->data[position] = 0;
    for (int i = position; i < l->index; i++)
    {
        l->data[i] = l->data[i + 1];
    }
    l->index--;
    return tmp;
}

/**
 * @brief recuperation de la valeur en fonction de la position
 * 
 * @param l 
 * @param position 
 * @return float 
 */
float get_at(Array_list *l, int position)
{
    return l->data[position];
}

/**
 * @brief vidage de la liste
 * 
 * @param l 
 */
void clear_array(Array_list *l){
    init_array_list(l);
}