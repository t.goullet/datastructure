
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "stack.h"
#include "queue.h"
#include "array_list.h"
#include "heap.h"

#include <CUnit/CUnit.h> 
#include <CUnit/Basic.h>



/**
 * @brief test des fonction de stack avec insertion 
 * 
 */
void test_stack()
{
    printf("Lancement des tests stacks ! \n");
    Stack *s = malloc(1 * sizeof(Stack));
    init_stack(s);
    push_stack(s, 1);
    push_stack(s, 2);
    push_stack(s, 3);
    assert(peek_stack(s) == 3);
    swap(s);
    assert(pop_stack(s) == 1);
    dup(s);
    assert(peek_stack(s) == 3);
    clear_stack(s);
    assert(is_stack_empty(s) == 1);
}

/**
 * @brief test des fonction de stack avec c-Unit
 * 
 */
void test_stack1()
{
    printf("Lancement des tests stacks ! \n");
    Stack *s = malloc(1 * sizeof(Stack));
    init_stack(s);
    push_stack(s, 1);
    push_stack(s, 2);
    push_stack(s, 3);
    CU_ASSERT_EQUAL(peek_stack(s),3);
    swap(s);
    CU_ASSERT_EQUAL(pop_stack(s),1);
    dup(s);
    CU_ASSERT_EQUAL(peek_stack(s),3);
    clear_stack(s);
    CU_ASSERT_EQUAL(is_stack_empty(s),1);
}

/**
 * @brief test des fonctions de queue avec insertion
 * 
 */
void test_queue()
{
    printf("Lancement des tests queue ! \n");
    Queue *q = malloc(1 * sizeof(Queue));
    init_queue(q);
    enqueue(q, 1);
    assert(front(q) == 1);
    dequeue(q);
     assert(front(q) == 0);
    clearqueue(q);
    assert(is_queue_empty(q) == 1);
}

/**
 * @brief test des fonctions de queu c-unit
 * 
 */
void test_queue1()
{
    printf("Lancement des tests queue ! \n");
    Queue *q = malloc(1 * sizeof(Queue));
    init_queue(q);
    enqueue(q, 1);
    CU_ASSERT_EQUAL(front(q),1);
    dequeue(q);
    CU_ASSERT_EQUAL(front(q),0);
    clearqueue(q);
    CU_ASSERT_EQUAL(is_queue_empty(q),1);
}


/**
 * @brief test des fonctions de array avec insertion 
 * 
 */
void test_arraylist()
{
    printf("Lancement des tests arraylist ! \n");
    Array_list *l = malloc(1 * sizeof(Array_list));
    init_array_list(l);
    add_list(l,1);
    add_list(l,2);
    assert(get_at(l, 1) == 2);
    insert_at(l, 1, 5);
    assert(get_at(l, 1) == 5);
    remove_at(l, 0);
    assert(get_at(l, 0) == 5);

}

/**
 * @brief test des fonctions de array avec c-unit
 * 
 */
void test_arraylist1()
{
    printf("Lancement des tests arraylist ! \n");
    Array_list *l = malloc(1 * sizeof(Array_list));
    init_array_list(l);
    add_list(l,1);
    add_list(l,2);
    CU_ASSERT_EQUAL(get_at(l, 1),2);
    insert_at(l, 1, 5);
    CU_ASSERT_EQUAL(get_at(l, 1),5);
    remove_at(l, 0);
    CU_ASSERT_EQUAL(get_at(l, 0),5);

}


/**
 * @brief test des fonctions de heap avec insertion 
 * 
 */
void test_heap()
{
    printf("Lancement des tests arraylist ! \n");
      Heap *heap = malloc(1 * sizeof(Heap));
    init_heap(heap);
    assert(is_heap_empty(heap) == true);
    push_heap(heap, 3);
    push_heap(heap, 2);
    push_heap(heap, 1);
    assert(is_heap_empty(heap) == false);
    assert(peek_heap(heap) == 1);
    replace(heap, 4);
    assert(peek_heap(heap) == 4);
    pop_heap(heap);
    assert(peek_heap(heap) == 2);
    clear_heap(heap);
    assert(is_heap_empty(heap) == true);
    printf("Success !\n");
    printf("Ending heap tests.\n");
    printf("**\n\n\n");
}

/**
 * @brief Test des fonctions de heap avec c-Unit
 */
void test_heap1()
{
    Heap *heap = (Heap *)malloc(sizeof(Heap));
    init_heap(heap);
    CU_ASSERT_TRUE(is_heap_empty(heap));
    push_heap(heap, 3);
    push_heap(heap, 2);
    push_heap(heap, 1);
    CU_ASSERT_FALSE(is_heap_empty(heap));
    CU_ASSERT(peek_heap(heap) == 1)
    replace(heap, 4);
    CU_ASSERT(peek_heap(heap) == 4)
    pop_heap(heap);
    CU_ASSERT(peek_heap(heap) == 2)
    clear_heap(heap);
    CU_ASSERT_TRUE(is_heap_empty(heap));
}


int init(void){

    return 0;
}

int clear_up(void){
    return 0;
}

int main(int argc, char **argv)
{
    // test_stack();
    // test_queue();
    // test_arraylist();
    // test_heap();

     CU_initialize_registry();
     CU_Suite *suite = CU_add_suite("tests",init,clear_up);
     CU_add_test(suite, "test stack",test_stack1);
     CU_add_test(suite, "test queue",test_queue1);
     CU_add_test(suite, "test arraylist",test_arraylist1);
     CU_add_test(suite, "test heap",test_heap1);
     CU_basic_run_tests();

    return (EXIT_SUCCESS);
}
