#include <stdbool.h>
#include "queue.h"

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @brief initialisation d'une file
 * 
 * @param q 
 */
void init_queue(Queue *q){
    q->index = 0; 
}

/**
 * @brief ajout d'une valeur dans la file 
 * 
 * @param q 
 * @param value 
 */
void enqueue(Queue *q, float value){
    (*q).data[(*q).index] = value;
    (*q).index--;
}

/**
 * @brief retier la premiere valeur de la queue
 * 
 * @param q 
 * @return float 
 */
float dequeue(Queue *q){
    (*q).index++;
    return (*q).data[(*q).index];
}

/**
 * @brief verifie si la queue est remplie
 * 
 * @param q 
 * @return true 
 * @return false 
 */
bool is_queue_empty(Queue *q){
    return q->index == 0;
}

/**
 * @brief sort la premiere valeur de la file sans la supprimer
 * 
 * 
 * @param q 
 * @return float 
 */
float front(Queue *q){ //aka front
    return (*q).data[(*q).index-1];
} 

/**
 * @brief vifde la file
 * 
 * @param q 
 */
void clearqueue(Queue *q){
    init_queue(q);
}