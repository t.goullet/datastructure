/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief initialise d'une pile
 * 
 * @param s 
 */
void init_stack(Stack *s){
    s->index = 0;
}

/**
 * @brief ajoute une valeur a la pile
 * 
 * @param s 
 * @param value 
 */
void push_stack(Stack *s, float value) {
    (*s).data[(*s).index] = value;
    (*s).index++;
}

/**
 * @brief supprime la dernière valeur de la file
 * 
 * @param s 
 * @return float 
 */
float pop_stack(Stack *s){
    (*s).index--;
    return (*s).data[(*s).index];  
}

/**
 * @brief verifie la disponibilité de la pile
 * 
 * @param s 
 * @return true 
 * @return false 
 */
bool is_stack_empty(Stack *s){
    if((*s).index == 0){
        return true;
    }
    else{
        return false;
    }
    
}

/**
 * @brief recupère la dernière valeur de la pile
 * 
 * @param s 
 * @return float 
 */
float peek_stack(Stack *s){
    return (*s).data[(*s).index-1];
}

/**
 * @brief duplique le sommet de la pile 
 * 
 * @param s 
 */
void dup(Stack *s){
    (*s).data[(*s).index] = (*s).data[(*s).index - 1];
    (*s).index++;
}

/**
 * @brief permet d'achanger les 2 valeurs au sommet de la pile
 * 
 * @param s 
 */
void swap(Stack *s)
{
    float tmp;
    if (s->index >= 1)
    {
        tmp = pop_stack(s);
        s->data[s->index-1] = s->data[0];
        s->data[0] = tmp;
    }
}

/**
 * @brief permet de vider la pile
 * 
 * @param s 
 */
void clear_stack(Stack *s)
{
    init_stack(s);
}